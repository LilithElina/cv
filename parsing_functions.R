# Regex to locate links in text
find_link <- regex("
  \\[   # Grab opening square bracket
  .+?   # Find smallest internal text as possible
  \\]   # Closing square bracket
  \\(   # Opening parenthesis
  .+?   # Link text, again as small as possible
  \\)   # Closing parenthesis
  ",
                   comments = TRUE)

# Function that removes links from text and replaces them with superscripts that are 
# referenced in an end-of-document list. 
sanitize_links <- function(text){
  if(PDF_EXPORT){
    str_extract_all(text, find_link) %>% 
      pluck(1) %>% 
      walk(function(link_from_text){
        title <- link_from_text %>% str_extract('\\[.+\\]') %>% str_remove_all('\\[|\\]') 
        link <- link_from_text %>% str_extract('\\(.+\\)') %>% str_remove_all('\\(|\\)')
        
        # add link to links array
        if (!(link%in%links)){
          links <<- c(links, link)
          # Build replacement text
          new_text <- glue('{title}<sup>{length(links)}</sup>')
        } else {
          ind <- match(link, links)
          new_text <- glue('{title}<sup>{ind}</sup>')
        }
        
        # Replace text
        text <<- text %>% str_replace(fixed(link_from_text), new_text)
      })
  }
  text
}

# Take entire positions dataframe and removes the links 
# in descending order so links for the same position are
# right next to eachother in number. 
strip_links_from_cols <- function(data, cols_to_strip){
  for(i in 1:nrow(data)){
    for(col in cols_to_strip){
      data[i, col] <- sanitize_links(data[i, col])
    }
  }
  data
}

# Take a position dataframe and the section id desired
# and prints the section to markdown. 
print_section <- function(position_data, section_id){
  position_data %>%
    filter(section==section_id) %>%
    replace(is.na(.), "") %>%
    arrange(desc(start_year)) %>%
    mutate(timeline=ifelse(start_month==end_month & start_year==end_year,
                           glue('{start_month} {start_year}'),
                           glue('{start_month} {start_year} - {end_month} {end_year}'))) %>%
    strip_links_from_cols("description") %>%
    glue_data(
      "### {title}",
      "\n\n",
      "{institute}",
      "\n\n",
      "{location}",
      "\n\n",
      "{timeline}",
      "\n\n",
      "{description}",
      "\n\n\n"
    )
}
