## My pagedown rendered CV

This repo contains the source-code and results of my CV built with the [pagedown package](https://pagedown.rbind.io) and a modified version of the 'resume' template. Most of the code comes from the [CV of Nick Strayer](https://github.com/nstrayer/cv). 

The main files are:

- `index.Rmd`: Source template for the CV, contains a variable `PDF_EXPORT` in the header that changes styles for PDF vs HTML. 
- `index.html`: The final output of the template when the header variable `PDF_EXPORT` is set to `FALSE`. View it at [sarah-pohl-cv.netlify.com](https://sarah-pohl-cv.netlify.com/).
- `sarah-pohl-cv.pdf`: The final exported PDF as rendered by Chrome. Links are put in the footer and notes about the online version are added. 
- `positions_spo12.csv`: A CSV with columns encoding the various fields needed for a position entry in the CV. A column `section` is also available so different sections know which rows to use.
- `publications.bib`: A bibliography file exported from Mendeley containing publications I have (co-)authored to be listed in the CV. It is parsed with the awesome [bib2df package](https://github.com/ropensci/bib2df) in `index.Rmd` - no manual curation of publications required!
- `css/`: Directory containing the custom CSS files used to tweak the default 'resume' format from pagedown.
